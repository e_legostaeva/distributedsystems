package ru.nsu.ccfit.legostaeva.task3.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.legostaeva.task3.model.entity.NodeEntity;
import ru.nsu.ccfit.legostaeva.task3.repository.NodeRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class NodeService {

    private final NodeRepository nodeRepository;

    public NodeEntity getNode(Long id) {
        return nodeRepository.findById(id).orElse(null);
    }

    public NodeEntity save(NodeEntity node) {
        return nodeRepository.save(node);
    }

    public NodeEntity update(NodeEntity node) {
        nodeRepository.findById(node.getId()).orElseThrow(NullPointerException::new);
        return nodeRepository.save(node);
    }

    public void delete(long id) {
        NodeEntity node = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        nodeRepository.delete(node);
    }

    public List<NodeEntity> findNodesInCoordsAndRadius(Double latitude, Double longitude, Double radius) {
        return nodeRepository.getNodesInCoordsAndRadius(latitude, longitude, radius);
    }
}
