package ru.nsu.ccfit.legostaeva.task3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.nsu.ccfit.legostaeva.task3.model.entity.NodeEntity;
import ru.nsu.ccfit.legostaeva.task3.model.entity.TagEntity;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class NodeDto {

    @NotNull
    private Long id;

    @NotNull
    private String user;

    @NotNull
    private Double longitude;

    @NotNull
    private Double latitude;

    @NotNull
    private Map<String, String> tags;

    public static NodeDto fromEntityToDto(NodeEntity nodeEntity) {
        Map<String, String> tags = nodeEntity.getTags().stream()
                .collect(Collectors.toMap(TagEntity::getKey, TagEntity::getValue));
        return new NodeDto(
                nodeEntity.getId(), nodeEntity.getUser(), nodeEntity.getLongitude(), nodeEntity.getLatitude(), tags
        );
    }
}
