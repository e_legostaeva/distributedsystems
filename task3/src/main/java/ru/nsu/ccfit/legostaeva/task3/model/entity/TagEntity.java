package ru.nsu.ccfit.legostaeva.task3.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nsu.ccfit.legostaeva.task3.model.TagId;
import ru.nsu.ccfit.legostaeva.task3.model.generated.Node;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tags")
@IdClass(TagId.class)
public class TagEntity {
    @Id
    private long id;

    @Id
    private String key;

    @Column(name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "node_id", insertable = false, updatable = false)
    private NodeEntity node;

    public TagEntity(long nodeId, String key, String value) {
        this.id = nodeId;
        this.key = key;
        this.value = value;
    }

    public static TagEntity fromTagToEntity(Node.Tag tag, long nodeId) {
        return new TagEntity(nodeId, tag.getK(), tag.getV());
    }

    public static TagEntity fromTagToEntity(String key, String value, long nodeId) {
        return new TagEntity(nodeId, key, value);
    }
}
