package ru.nsu.ccfit.legostaeva.task3.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nsu.ccfit.legostaeva.task3.dto.NodeDto;
import ru.nsu.ccfit.legostaeva.task3.model.generated.Node;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "nodes")
public class NodeEntity {
    @Id
    private long id;

    @Column(name = "username")
    private String user;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "node", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<TagEntity> tags;

    public NodeEntity(long id, String user, Double longitude, Double latitude) {
        this.id = id;
        this.user = user;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public static NodeEntity fromNodeToEntity(Node node) {
        List<TagEntity> tags = node.getTag().stream()
                .map(tag -> TagEntity.fromTagToEntity(tag, node.getId()))
                .collect(Collectors.toList());
        return new NodeEntity(node.getId(), node.getUser(), node.getLon(), node.getLat(), tags);
    }

    public static NodeEntity fromNodeToEntity(NodeDto nodeDto) {
        List<TagEntity> tags = nodeDto.getTags().entrySet().stream()
                .map(tag -> TagEntity.fromTagToEntity(tag.getKey(), tag.getValue(), nodeDto.getId()))
                .collect(Collectors.toList());
        return new NodeEntity(nodeDto.getId(), nodeDto.getUser(), nodeDto.getLongitude(), nodeDto.getLatitude(), tags);
    }
}
