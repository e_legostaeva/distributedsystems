package ru.nsu.ccfit.legostaeva.task3.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.legostaeva.task3.dto.NodeDto;
import ru.nsu.ccfit.legostaeva.task3.dto.SearchDto;
import ru.nsu.ccfit.legostaeva.task3.model.entity.NodeEntity;
import ru.nsu.ccfit.legostaeva.task3.service.NodeService;
import ru.nsu.ccfit.legostaeva.task3.service.OsmParsingService;

import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class NodeController {

    private final NodeService nodeService;
    private final OsmParsingService osmParsingService;

    @GetMapping("/init")
    public void init() throws JAXBException, IOException, XMLStreamException {
        osmParsingService.readFile();
    }

    @PutMapping("/save")
    public ResponseEntity<NodeDto> saveNode(@Valid @RequestBody NodeDto nodeDTO) {
        return new ResponseEntity<>(
                NodeDto.fromEntityToDto(nodeService.save(NodeEntity.fromNodeToEntity(nodeDTO))),
                HttpStatus.OK
        );
    }

    @GetMapping("/search")
    public ResponseEntity<List<NodeDto>> searchNodes(@Valid @RequestBody SearchDto searchDTO) {
        return new ResponseEntity<>(
                nodeService.findNodesInCoordsAndRadius(
                        searchDTO.getLatitude(), searchDTO.getLongitude(), searchDTO.getRadius()
                ).stream()
                        .map(NodeDto::fromEntityToDto)
                        .collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<NodeDto> getNode(@PathVariable("id") Long id) {
        NodeEntity nodeEntity = nodeService.getNode(id);
        if (nodeEntity != null) {
            return new ResponseEntity<>(NodeDto.fromEntityToDto(nodeEntity), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        nodeService.delete(id);
    }

    @PutMapping("/update")
    public ResponseEntity<NodeDto> updateNode(
            @Valid @RequestBody NodeDto node) {
        try {
            NodeEntity nodeEntity = nodeService.update(NodeEntity.fromNodeToEntity(node));
            return new ResponseEntity<>(NodeDto.fromEntityToDto(nodeEntity), HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
