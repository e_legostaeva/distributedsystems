package ru.nsu.ccfit.legostaeva.task3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.legostaeva.task3.model.entity.NodeEntity;

import java.util.List;

@Repository
public interface NodeRepository extends JpaRepository<NodeEntity, Long> {
    @Query(value = "SELECT * " +
            "FROM nodes " +
            "WHERE earth_distance( " +
            "   ll_to_earth(nodes.latitude, nodes.longitude), " +
            "   ll_to_earth(:latitude, :longitude) " +
            ") < :radius",
            nativeQuery = true)
    List<NodeEntity> getNodesInCoordsAndRadius(@Param("latitude") double latitude,
                                               @Param("longitude") double longitude,
                                               @Param("radius") double radius);
}
