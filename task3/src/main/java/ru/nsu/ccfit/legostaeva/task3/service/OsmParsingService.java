package ru.nsu.ccfit.legostaeva.task3.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.legostaeva.task3.model.entity.NodeEntity;
import ru.nsu.ccfit.legostaeva.task3.model.generated.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
@AllArgsConstructor
public class OsmParsingService {
    private static final Logger LOG = LoggerFactory.getLogger(OsmParsingService.class);
    private static final String NODE = "node";

    private final NodeService nodeProcessor;

    public void readFile() throws XMLStreamException, IOException, JAXBException {
        try (XmlStreamProcessor processor = new XmlStreamProcessor(Files.newInputStream(Paths.get("RU-NVS.osm")))) {
            JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            XMLStreamReader reader = processor.getReader();
            //while (reader.hasNext()) {
            for (int i = 0; i <= 100_000; ++i) {
                int event = reader.next();
                if (event == XMLEvent.START_ELEMENT && NODE.equals(reader.getLocalName())) {
                    Node node = (Node) unmarshaller.unmarshal(reader);
                    NodeEntity nodeEntity = NodeEntity.fromNodeToEntity(node);
                    nodeProcessor.save(nodeEntity);
                    LOG.info("Saved node with id {}", nodeEntity.getId());
                }
            }

        }
    }
}
