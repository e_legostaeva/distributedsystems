package ru.nsu.ccfit.legostaeva.task3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task3 {
    public static void main(String[] args) {
        SpringApplication.run(Task3.class);
    }
}
