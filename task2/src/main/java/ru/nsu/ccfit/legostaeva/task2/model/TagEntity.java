package ru.nsu.ccfit.legostaeva.task2.model;

import lombok.Data;
import ru.nsu.ccfit.legostaeva.task2.model.generated.Node;

@Data
public class TagEntity {
    private final long nodeId;
    private final String key;
    private final String value;

    public static TagEntity getFromNode(Node.Tag tag, long nodeId) {
        return new TagEntity(nodeId, tag.getK(), tag.getV());
    }
}
