package ru.nsu.ccfit.legostaeva.task2;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.ccfit.legostaeva.task2.model.NodeEntity;
import ru.nsu.ccfit.legostaeva.task2.model.generated.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

@AllArgsConstructor
public class OsmParser {
    private static final Logger LOG = LoggerFactory.getLogger(OsmParser.class);
    private static final String NODE = "node";

    private final NodeHandler nodeHandler;

    public void parse(InputStream inputStream) throws JAXBException, XMLStreamException {
        LOG.info("OSM processing start");
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        try {
            reader = factory.createXMLStreamReader(inputStream);
            long seconds = 0;
            //while (reader.hasNext()) {
            for (int i = 0; i <= 100_000; ++i) {
                int event = reader.next();
                if (XMLStreamConstants.START_ELEMENT == event && NODE.equals(reader.getLocalName())) {
                    long curSec = System.currentTimeMillis();
                    parseNode(jaxbContext, reader);
                    curSec = System.currentTimeMillis() - curSec;
                    seconds += curSec;
                }
            }
            System.out.println("Всего секунд на вставку " + seconds / 1000);
            double perOne = (seconds / (100000.0));
            System.out.println("Миллисекунд на объект " + perOne);
            System.out.println("Объектов в секунду " + 1.0 / perOne * 1000);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        nodeHandler.flush();
        LOG.info("OSM processing finish");
    }

    private void parseNode(JAXBContext jaxbContext, XMLStreamReader reader) throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Node node = (Node) unmarshaller.unmarshal(reader);
        nodeHandler.putNode(NodeEntity.getFromNode(node));
    }

}
