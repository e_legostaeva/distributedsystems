package ru.nsu.ccfit.legostaeva.task2.model;

import lombok.Data;
import ru.nsu.ccfit.legostaeva.task2.model.generated.Node;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class NodeEntity {
    private long id;
    private String user;
    private Double longitude;
    private Double latitude;
    private List<TagEntity> tags;

    public NodeEntity(long id, String user, Double longitude, Double latitude, List<TagEntity> tags) {
        this.id = id;
        this.user = user;
        this.longitude = longitude;
        this.latitude = latitude;
        this.tags = tags;
    }

    public NodeEntity(long id, String user, Double longitude, Double latitude) {
        this.id = id;
        this.user = user;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public static NodeEntity getFromNode(Node node) {
        List<TagEntity> tags = node.getTag().stream()
                .map(tag -> TagEntity.getFromNode(tag, node.getId()))
                .collect(Collectors.toList());
        return new NodeEntity(node.getId(), node.getUser(), node.getLon(), node.getLat(), tags);
    }
}
