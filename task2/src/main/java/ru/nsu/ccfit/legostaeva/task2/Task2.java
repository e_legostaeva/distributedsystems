package ru.nsu.ccfit.legostaeva.task2;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.ccfit.legostaeva.task2.dao.NodeDao;
import ru.nsu.ccfit.legostaeva.task2.dao.NodeDaoImpl;
import ru.nsu.ccfit.legostaeva.task2.dao.TagDao;
import ru.nsu.ccfit.legostaeva.task2.dao.TagDaoImpl;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

public class Task2 {
    private static final Logger LOG = LoggerFactory.getLogger(Task2.class);

    public static void main(String[] args) throws SQLException {
        LOG.info("task 2!");

        if (args.length < 1) {
            throw new IllegalArgumentException("Wrong argument amount. Please, add file with permission .osm.bz2");
        }

        try (InputStream inputStream = new BZip2CompressorInputStream(new FileInputStream(args[0]))) {
            DBConnection.init();
            NodeDao nodeDao = new NodeDaoImpl();
            TagDao tagDao = new TagDaoImpl();
            NodeHandler nodeHandler = new NodeHandler(nodeDao, tagDao);
            OsmParser osmParser = new OsmParser(nodeHandler);
            osmParser.parse(inputStream);
        } catch (FileNotFoundException e) {
            LOG.error("File not found", e);
        } catch (IOException e) {
            LOG.error("File read error", e);
        } catch (JAXBException | XMLStreamException e) {
            LOG.error("File parsing error", e);
        } catch (SQLException e) {
            LOG.error("Failed to initialize database", e);
        } finally {
            DBConnection.closeConnection();
        }
    }
}
