package ru.nsu.ccfit.legostaeva.task2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
    public static final String URL = "jdbc:postgresql://localhost:5433/osm";
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "tolyalalka";
    private static final String INIT_SQL_PATH = "init.sql";
    private static Connection connection;

    public static void init() throws IOException, SQLException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(INIT_SQL_PATH).getFile());
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            StringBuffer stringBuffer = new StringBuffer("");
            while (reader.ready()) {
                stringBuffer.append(reader.readLine());
            }
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(new String(stringBuffer));
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
}
