package ru.nsu.ccfit.legostaeva.task2.dao;

import ru.nsu.ccfit.legostaeva.task2.DBConnection;
import ru.nsu.ccfit.legostaeva.task2.model.NodeEntity;

import java.sql.*;
import java.util.List;

public class NodeDaoImpl implements NodeDao {
    private static final String SQL_GET = "" +
            "select id, username, longitude, latitude " +
            "from nodes " +
            "where id = ?";

    private static final String SQL_INSERT = "" +
            "insert into nodes(id, username, longitude, latitude) " +
            "values (?, ?, ?, ?)";

    private final PreparedStatement preparedStatementInsert;
    private final PreparedStatement preparedStatementGet;

    public NodeDaoImpl() throws SQLException {
        Connection connection = DBConnection.getConnection();
        this.preparedStatementInsert = connection.prepareStatement(SQL_INSERT);
        this.preparedStatementGet = connection.prepareStatement(SQL_GET);
    }

    private static void prepareStatement(PreparedStatement statement, NodeEntity node) throws SQLException {
        statement.setLong(1, node.getId());
        statement.setString(2, node.getUser());
        statement.setDouble(3, node.getLongitude());
        statement.setDouble(4, node.getLatitude());
    }

    private static NodeEntity mapNode(ResultSet rs) throws SQLException {
        return new NodeEntity(rs.getLong("id"), rs.getString("username"),
                rs.getDouble("longitude"), rs.getDouble("latitude"));
    }

    @Override
    public NodeEntity getNode(long nodeId) throws SQLException {
        preparedStatementGet.setLong(1, nodeId);
        ResultSet resultSet = preparedStatementGet.executeQuery(SQL_GET);
        return resultSet.next() ? mapNode(resultSet) : null;
    }

    @Override
    public void insertNode(NodeEntity node) throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement statement = connection.createStatement();
        String sql = "insert into nodes(id, username, longitude, latitude) " +
                "values (" + node.getId() + ", '" + node.getUser().replaceAll("'","''") + "', " + node.getLongitude() +
                ", " + node.getLatitude() + ")";
        statement.execute(sql);
    }

    @Override
    public void insertPreparedNode(NodeEntity node) throws SQLException {
        prepareStatement(preparedStatementInsert, node);
        preparedStatementInsert.execute();
    }

    @Override
    public void batchInsertNodes(List<NodeEntity> nodes) throws SQLException {
        for (NodeEntity node : nodes) {
            prepareStatement(preparedStatementInsert, node);
            preparedStatementInsert.addBatch();
        }
        preparedStatementInsert.executeBatch();
    }
}
