package ru.nsu.ccfit.legostaeva.Task1;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Wrong argument amount. Please, add file with permission .osm.bz2");
        }

        LOG.info("File handling start...");
        try (InputStream inputStream = new BZip2CompressorInputStream(new FileInputStream(args[0]))) {
            LOG.info("Osm parsing process started...");
            OsmParserResult result = OsmParser.parse(inputStream);
            LOG.info("Osm parsing process end.");
            System.out.println("User names and changes amount:");
            result.getUsers().entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
            System.out.println("---------------------------------------------------");
            System.out.println("Tags amount:");
            result.getTags().forEach((key, value) -> System.out.println(key + " : " + value));
        } catch (FileNotFoundException e) {
            LOG.error("File not found", e);
        } catch (IOException e) {
            LOG.error("Error while reading file", e);
        } catch (XMLStreamException e) {
            LOG.error("Error while reading XML", e);
        }
    }
}
