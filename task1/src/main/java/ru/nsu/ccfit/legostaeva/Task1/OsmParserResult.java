package ru.nsu.ccfit.legostaeva.Task1;

import java.util.HashMap;
import java.util.Map;

public class OsmParserResult {
    private final Map<String, Integer> users = new HashMap<>();
    private final Map<String, Integer> tags = new HashMap<>();

    public Map<String, Integer> getUsers() {
        return users;
    }

    public Map<String, Integer> getTags() {
        return tags;
    }

    public void editUsers(String user){
        users.merge(user, 1, Integer::sum);
    }

    public void editTags(String tag){
        tags.merge(tag, 1, Integer::sum);
    }
}
