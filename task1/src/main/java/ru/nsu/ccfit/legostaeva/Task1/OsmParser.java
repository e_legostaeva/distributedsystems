package ru.nsu.ccfit.legostaeva.Task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;

public class OsmParser {
    private static final Logger LOG = LoggerFactory.getLogger(OsmParser.class);

    private static final QName USER = new QName("user");
    private static final QName ID = new QName("id");
    private static final QName KEY = new QName("k");

    private OsmParser() {
        throw new IllegalAccessError();
    }

    public static OsmParserResult parse(InputStream inputStream) throws XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        OsmParserResult result = new OsmParserResult();
        XMLEventReader eventReader = null;
        try {
            eventReader = inputFactory.createXMLEventReader(inputStream);
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (XMLStreamConstants.START_ELEMENT == event.getEventType()) {
                    StartElement startElement = event.asStartElement();
                    if ("node".equals(startElement.getName().getLocalPart())) {
                        Attribute userAttribute = startElement.getAttributeByName(USER);
                        LOG.debug("Parse user with name {}", userAttribute.getValue());
                        result.editUsers(userAttribute.getValue());
                        Attribute idAttribute = startElement.getAttributeByName(ID);
                        LOG.debug("Parse tag in node with id {}", idAttribute);
                        parseTags(result, eventReader);
                    }
                }
            }
        } finally {
            if (eventReader != null) {
                eventReader.close();
            }
        }
        return result;
    }

    private static void parseTags(OsmParserResult result,
                                  XMLEventReader eventReader) throws XMLStreamException {
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (XMLStreamConstants.END_ELEMENT == event.getEventType() &&
                    "node".equals(event.asEndElement().getName().getLocalPart())) {
                return;
            }
            if (XMLStreamConstants.START_ELEMENT == event.getEventType()) {
                StartElement startElement = event.asStartElement();
                if ("tag".equals(startElement.getName().getLocalPart())) {
                    Attribute key = startElement.getAttributeByName(KEY);
                    result.editTags(key.getValue());
                }
            }
        }
    }
}
